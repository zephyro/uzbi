<!doctype html>
<html class="no-js" lang="">
    <head>

        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->

        <link rel="stylesheet" href="css/product.css">

    </head>
    <body>

        <div class="layout">

            <!-- Location -->
            <?php include('inc/location.inc.php') ?>
            <!-- -->

            <!-- Mobile Nav -->
            <?php include('inc/mobile_nav.inc.php') ?>
            <!-- -->

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <div class="page">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <section class="main">
                    <div class="container">

                        <ul class="breadcrumb">
                            <li><a href="#">Главная</a></li>
                            <li><a href="#">Каталог</a></li>
                            <li><span>Электросушилка Ветерок-5, прозрачный</span></li>
                        </ul>

                        <div class="product">

                            <h1 class="product_title"><span class="color_green">Э</span>лектросушилка Ветерок-5, прозрачный</h1>
                            <div class="product_subtitle">Сушилки для овощей, фруктов</div>

                            <div class="product__row">

                                <div class="product__media">

                                    <div class="product__media_thumbs">

                                        <div class="product__media_container">
                                            <div class="gallery_thumb swiper-container">
                                                <div class=" swiper-wrapper">
                                                    <div class="swiper-slide">
                                                        <a class="product__media_thumb active" href="#" title="title" data-target="0">
                                                            <img src="images/product_xl.jpg" alt="title" title="title" class="img-fluid">
                                                        </a>
                                                    </div>
                                                    <div class="swiper-slide">
                                                        <a class="product__media_thumb" href="#" title="title" data-target="1">
                                                            <img src="images/product_xl.jpg" alt="title" title="title" class="img-fluid">
                                                        </a>
                                                    </div>
                                                    <div class="swiper-slide">
                                                        <a class="product__media_thumb" href="#" title="title" data-target="2">
                                                            <img src="images/product_xl.jpg" alt="title" title="title" class="img-fluid">
                                                        </a>
                                                    </div>
                                                    <div class="swiper-slide">
                                                        <a class="product__media_thumb" href="#" title="title" data-target="3">
                                                            <img src="images/product_xl.jpg" alt="title" title="title" class="img-fluid">
                                                        </a>
                                                    </div>
                                                    <div class="swiper-slide">
                                                        <a class="product__media_thumb" href="#" title="title" data-target="4">
                                                            <img src="images/product_xl.jpg" alt="title" title="title" class="img-fluid">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="thumb_arrow_prev"></span>
                                            <span class="thumb_arrow_next"></span>
                                        </div>

                                    </div>
                                    <div class="product__media_gallery">
                                        <div class="gallery_slider swiper-container">
                                            <div class=" swiper-wrapper">
                                                <div class="swiper-slide">
                                                    <a class="product__media_image" href="images/product_xl.jpg" title="title" data-fancybox="gallery">
                                                        <img src="images/product_xl.jpg" alt="title" title="title" class="img-fluid">
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a class="product__media_image" href="images/product_xl.jpg" title="title" data-fancybox="gallery">
                                                        <img src="images/product_xl.jpg" alt="title" title="title" class="img-fluid">
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a class="product__media_image" href="images/product_xl.jpg" title="title" data-fancybox="gallery">
                                                        <img src="images/product_xl.jpg" alt="title" title="title" class="img-fluid">
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a class="product__media_image" href="images/product_xl.jpg" title="title" data-fancybox="gallery">
                                                        <img src="images/product_xl.jpg" alt="title" title="title" class="img-fluid">
                                                    </a>
                                                </div>
                                                <div class="swiper-slide">
                                                    <a class="product__media_image" href="images/product_xl.jpg" title="title" data-fancybox="gallery">
                                                        <img src="images/product_xl.jpg" alt="title" title="title" class="img-fluid">
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- Add Pagination -->
                                            <div class="swiper-pagination"></div>
                                        </div>
                                    </div>

                                </div>

                                <div class="product__info">

                                    <div class="product__info_heading">
                                        <a class="product__info_favorite">
                                            <img src="img/icon__favorites.svg" class="img-fluid" alt="">
                                        </a>
                                        <a class="product__info_comparison">
                                            <img src="img/icon__comparison.png" class="img-fluid" alt="">
                                        </a>
                                        <div class="product__info_logo">
                                            <img src="img/icon__logo_small.png" class="img-fluid" alt="">
                                        </div>
                                    </div>

                                    <div class="product__cost">

                                        <div class="product__meta">
                                            <div class="product__meta_item product__meta_blue">уценка</div>
                                            <div class="product__meta_item product__meta_green">хит</div>
                                            <div class="product__meta_item product__meta_yellow">Акция</div>
                                        </div>

                                        <div class="product__price">
                                            <div class="product__price_old">6 499</div>
                                            <div class="product__price_new">5 849₽ </div>
                                            <div class="product__price_discount">-10%</div>
                                        </div>

                                        <button type="button" class="btn">купить</button>

                                    </div>

                                    <div class="product__availability">
                                        <div class="product__availability_title">Наличие сейчас в <strong>Челябинске</strong></div>
                                        <ul class="product__availability_point">

                                            <li>2 шт. - <a href="#">ТРЦ Горки</a></li>
                                            <li class="not_available">нет -  ТРЦ Фокус</li>
                                            <li class="not_available">нет -  в пути (1-2 дня)</li>
                                            <li>1 шт. -  <a href="#">склад (1-4 дня)</a></li>
                                            <li>много -  <a href="#">удаленный склад (4-7 дней)</a></li>
                                        </ul>
                                        <a href="#" class="product__availability_amount">21 пункт выдачи</a>
                                    </div>

                                    <div class="product__delivery">
                                        <a href="#" class="product__delivery_calculation"><span>Рассчитать доставку</span></a>
                                        <ul class="product__delivery_list">
                                            <li><strong>Доставка в <a href="#">Екатеринбург</a></strong></li>
                                            <li><a href="#">Самовывоз</a> (5 дней) - <span class="color_green">Бесплатно</span></li>
                                            <li><a href="#">Доставка</a> (3дня) - <span class="color_gray">150₽</span></li>
                                        </ul>
                                    </div>
                                    <div class="product__bottom">
                                        <a href="#" class="btn btn_border_green">узнать о снижении цены</a>
                                    </div>

                                </div>

                            </div>

                            <div class="product_tabs">
                                <ul class="product_tabs__nav">
                                   <li class="active">
                                       <a href="#tab1">
                                           <b class="icon_cod">
                                               <svg class="ico-svg" viewBox="0 0 19 19" xmlns="http://www.w3.org/2000/svg">
                                                   <use xlink:href="img/sprite_icons.svg#icon__cog" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                               </svg>
                                           </b>
                                           <span>Характеристики</span>
                                       </a>
                                   </li>
                                    <li>
                                        <a href="#tab2">
                                            <b class="icon_doc">
                                                <svg class="ico-svg" viewBox="0 0 16 21" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__doc" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </b>
                                            <span>ОПИСАНИЕ ТОВАРА</span>
                                        </a>
                                    </li>
                                </ul>

                                <div class="product_tabs__content">

                                    <div class="product_tabs__item active" id="tab1">
                                        <div class="product_data active">
                                            <div class="product_data__heading">
                                                <b class="icon_cod">
                                                    <svg class="ico-svg" viewBox="0 0 19 19" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__cog" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </b>
                                                <span>Характеристики</span>
                                                <i class="fas fa-angle-down"></i>
                                            </div>
                                            <div class="product_data__content">

                                                <div class="product_data__row">

                                                    <div class="product_data__params">
                                                        <div class="product_data__hide">
                                                            <table class="params_table">
                                                                <tr>
                                                                    <td><span>Объем:</span></td>
                                                                    <td><span>25 л</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Число поддонов:</span></td>
                                                                    <td><span>5</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Материал</span></td>
                                                                    <td><span>Пластик</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Мощность:</span></td>
                                                                    <td><span>500 Вт</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Управление:</span></td>
                                                                    <td><span>Механическое</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Материал поддонов:</span></td>
                                                                    <td><span>Пластик</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Температура сушки:</span></td>
                                                                    <td><span>30/70 C°</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Тип сушки:</span></td>
                                                                    <td><span>Сушилка</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Максимальная загрузка:</span></td>
                                                                    <td><span>5 кг</span></td>
                                                                </tr>

                                                                <tr>
                                                                    <td><span>Объем:</span></td>
                                                                    <td><span>25 л</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Число поддонов:</span></td>
                                                                    <td><span>5</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Материал</span></td>
                                                                    <td><span>Пластик</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Мощность:</span></td>
                                                                    <td><span>500 Вт</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Управление:</span></td>
                                                                    <td><span>Механическое</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Материал поддонов:</span></td>
                                                                    <td><span>Пластик</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Температура сушки:</span></td>
                                                                    <td><span>30/70 C°</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Тип сушки:</span></td>
                                                                    <td><span>Сушилка</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><span>Максимальная загрузка:</span></td>
                                                                    <td><span>5 кг</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <a href="#" class="product_data__toggle">
                                                            <span class="open_flag">Показать еще...</span>
                                                            <span class="close_flag">Скрыть</span>
                                                            <i class="fas fa-angle-down"></i>
                                                        </a>
                                                    </div>
                                                    <div class="product_data__video">
                                                        <div class="embed-responsive embed-responsive-16by9">
                                                            <iframe src="https://www.youtube.com/embed/flONeWegrgs" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="product_tabs__item" id="tab2">
                                        <div class="product_data">
                                            <div class="product_data__heading">
                                                <b class="icon_doc">
                                                    <svg class="ico-svg" viewBox="0 0 16 21" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__doc" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </b>
                                                <span>ОПИСАНИЕ ТОВАРА</span>
                                                <i class="fas fa-angle-down"></i>
                                            </div>
                                            <div class="product_data__content">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse aliquet sem sit amet tellus blandit, in pretium enim blandit. Nunc tempus lacus ut sapien volutpat, sit amet pretium erat porta. Ut varius et dui in vestibulum. Fusce accumsan, ligula at dapibus euismod, dui mauris eleifend arcu, vel suscipit ipsum justo et diam. Ut vel augue nec dui dapibus iaculis. Sed eu justo et quam ultricies tempor. Mauris eleifend, tortor in aliquet efficitur, justo metus ultrices neque, nec placerat neque nibh sed libero. Duis commodo cursus felis sed tempor. Fusce varius elit justo, ut tempus augue pellentesque vitae. Integer tristique id risus sit amet vestibulum. Morbi dapibus leo in dolor aliquam, id interdum massa pellentesque. Sed at lacinia sem, in fermentum nulla. Suspendisse non nisl faucibus elit vestibulum pharetra ac sed felis.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </section>

                <section class="showcase">
                    <div class="container">
                        <div class="h2"><span class="color_green">С</span>опутствующие товары<sup class="color_green">19</sup></div>
                    </div>
                    <div class="showcase__slider_wrap">
                        <div class="container">
                            <div class="showcase__wrap">
                                <div class="showcase__slider showcase__slider_two swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                    <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                    <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                    <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                    <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="slider_arrow_prev">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="slider_arrow_next">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__angle_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="review">
                    <div class="container">
                        <div class="h2"><span class="color_green">О</span>тзывы</div>
                    </div>
                    <div class="showcase__slider_wrap">
                        <div class="container">
                            <div class="review__wrap">
                                <div class="review_slider swiper-container">
                                    <div class="swiper-wrapper">

                                        <div class="swiper-slide review_elem">
                                            <div class="review__item">
                                                <div class="review__name">
                                                    <strong>Александр Трухин</strong>
                                                    <span>07.09.2018 06:05:56</span>
                                                </div>
                                                <div class="review__text">
                                                    Добрый день.<br/>
                                                    Приобрел коптилью горячего копчения Аромат для приготовления курицы и рульки в домашних
                                                </div>
                                                <a href="#" class="review__link">подробнее ...</a>
                                            </div>
                                        </div>

                                        <div class="swiper-slide review_elem">
                                            <div class="review__item">
                                                <div class="review__name">
                                                    <strong>Александр Трухин</strong>
                                                    <span>07.09.2018 06:05:56</span>
                                                </div>
                                                <div class="review__text">
                                                    Добрый день.<br/>
                                                    Приобрел коптилью горячего копчения Аромат для приготовления курицы и рульки в домашних
                                                </div>
                                                <a href="#" class="review__link">подробнее ...</a>
                                            </div>
                                        </div>

                                        <div class="swiper-slide review_elem">
                                            <div class="review__item">
                                                <div class="review__name">
                                                    <strong>Александр Трухин</strong>
                                                    <span>07.09.2018 06:05:56</span>
                                                </div>
                                                <div class="review__text">
                                                    Добрый день.<br/>
                                                    Приобрел коптилью горячего копчения Аромат для приготовления курицы и рульки в домашних
                                                </div>
                                                <a href="#" class="review__link">подробнее ...</a>
                                            </div>
                                        </div>

                                        <div class="swiper-slide review_elem">
                                            <div class="review__item">
                                                <div class="review__name">
                                                    <strong>Александр Трухин</strong>
                                                    <span>07.09.2018 06:05:56</span>
                                                </div>
                                                <div class="review__text">
                                                    Добрый день.<br/>
                                                    Приобрел коптилью горячего копчения Аромат для приготовления курицы и рульки в домашних
                                                </div>
                                                <a href="#" class="review__link">подробнее ...</a>
                                            </div>
                                        </div>

                                        <div class="swiper-slide review_elem">
                                            <div class="review__item">
                                                <div class="review__name">
                                                    <strong>Александр Трухин</strong>
                                                    <span>07.09.2018 06:05:56</span>
                                                </div>
                                                <div class="review__text">
                                                    Добрый день.<br/>
                                                    Приобрел коптилью горячего копчения Аромат для приготовления курицы и рульки в домашних
                                                </div>
                                                <a href="#" class="review__link">подробнее ...</a>
                                            </div>
                                        </div>

                                        <div class="swiper-slide review_elem">
                                            <div class="review__item">
                                                <div class="review__name">
                                                    <strong>Александр Трухин</strong>
                                                    <span>07.09.2018 06:05:56</span>
                                                </div>
                                                <div class="review__text">
                                                    Добрый день.<br/>
                                                    Приобрел коптилью горячего копчения Аромат для приготовления курицы и рульки в домашних
                                                </div>
                                                <a href="#" class="review__link">подробнее ...</a>
                                            </div>
                                        </div>

                                        <div class="swiper-slide review_elem">
                                            <div class="review__item">
                                                <div class="review__name">
                                                    <strong>Александр Трухин</strong>
                                                    <span>07.09.2018 06:05:56</span>
                                                </div>
                                                <div class="review__text">
                                                    Добрый день.<br/>
                                                    Приобрел коптилью горячего копчения Аромат для приготовления курицы и рульки в домашних
                                                </div>
                                                <a href="#" class="review__link">подробнее ...</a>
                                            </div>
                                        </div>

                                        <div class="swiper-slide review_elem">
                                            <div class="review__item">
                                                <div class="review__name">
                                                    <strong>Александр Трухин</strong>
                                                    <span>07.09.2018 06:05:56</span>
                                                </div>
                                                <div class="review__text">
                                                    Добрый день.<br/>
                                                    Приобрел коптилью горячего копчения Аромат для приготовления курицы и рульки в домашних
                                                </div>
                                                <a href="#" class="review__link">подробнее ...</a>
                                            </div>
                                        </div>

                                        <div class="swiper-slide review_elem">
                                            <div class="review__item">
                                                <div class="review__name">
                                                    <strong>Александр Трухин</strong>
                                                    <span>07.09.2018 06:05:56</span>
                                                </div>
                                                <div class="review__text">
                                                    Добрый день.<br/>
                                                    Приобрел коптилью горячего копчения Аромат для приготовления курицы и рульки в домашних
                                                </div>
                                                <a href="#" class="review__link">подробнее ...</a>
                                            </div>
                                        </div>

                                        <div class="swiper-slide review_elem">
                                            <div class="review__item">
                                                <div class="review__name">
                                                    <strong>Александр Трухин</strong>
                                                    <span>07.09.2018 06:05:56</span>
                                                </div>
                                                <div class="review__text">
                                                    Добрый день.<br/>
                                                    Приобрел коптилью горячего копчения Аромат для приготовления курицы и рульки в домашних
                                                </div>
                                                <a href="#" class="review__link">подробнее ...</a>
                                            </div>
                                        </div>


                                    </div>
                                    <!-- Add Arrows -->
                                    <!-- Add Arrows -->
                                    <div class="slider_arrow_prev">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="slider_arrow_next">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__angle_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section class="showcase">
                    <div class="container">
                        <div class="h2"><span class="color_green">Р</span>екомендуемые товары<sup class="color_green">30</sup></div>
                    </div>
                    <div class="showcase__slider_wrap">
                        <div class="container">
                            <div class="showcase__wrap">
                                <div class="showcase__slider showcase__slider_two swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                    <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                    <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                    <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                    <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="slider_arrow_prev">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="slider_arrow_next">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__angle_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section class="main">
                    <div class="container">
                        <div class="h2"><span class="color_green"></span>Духовые шкафы</div>

                        <p>Не следует, однако забывать, что консультация с широким активом играет важную роль в формировании новых предложений. С другой стороны рамки и место обучения кадров влечет за собой процесс внедрения и модернизации новых предложений. Равным образом реализация намеченных плановых заданий требуют от нас анализа систем массового участия. Разнообразный и богатый опыт начало повседневной работы по формированию позиции играет важную роль в формировании модели развития. Разнообразный и богатый опыт новая модель организационной деятельности позволяет выполнять важные задания по разработке позиций, занимаемых участниками в отношении поставленных задач. Идейные соображения высшего порядка, а также реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании направлений прогрессивного развития.</p>
                        <p>Идейные соображения высшего порядка, а также консультация с широким активом способствует подготовки и реализации систем массового участия. Повседневная практика показывает, что реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий. Повседневная практика показывает, что новая модель организационной деятельности требуют от нас анализа систем массового участия. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности обеспечивает широкому кругу (специалистов) участие в формировании соответствующий условий активизации. Равным образом укрепление и развитие структуры позволяет выполнять важные задания по разработке систем массового участия. Разнообразный и богатый опыт постоянное информационно-пропагандистское обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации новых предложений.</p>

                    </div>
                </section>


                <!-- Contacts -->
                <?php include('inc/contacts.inc.php') ?>
                <!-- -->

                <!-- Footer -->
                <?php include('inc/footer.inc.php') ?>
                <!-- -->

            </div>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
