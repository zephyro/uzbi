<section class="contacts">
    <div class="contacts__heading">
        <div class="container">
            <div class="h2"><span class="color_green">А</span>дреса магазинов</div>
        </div>
    </div>
    <div class="contacts__wrap">
        <div class="container">
            <div class="contacts__info">
                <ul>
                    <li>
                        <div class="contacts__info_name">ТРК "ФИЕСТА", ОТДЕЛ "УЗБИ" 2 ЭТАЖ</div>
                        <div class="contacts__info_address">Россия, г. Челябинск, <br/>ул Молодогвардейцев, 7</div>
                        <div class="contacts__info_worktime">Время работы: ежедневно с 10:00 - 22:00</div>
                    </li>
                    <li>
                        <div class="contacts__info_name">ТРК "РОДНИК", ОТДЕЛ "УЗБИ" 2 ЭТАЖ</div>
                        <div class="contacts__info_address">Россия, г. Челябинск, ул Труда, 203</div>
                        <div class="contacts__info_worktime">Время работы: ежедневно с 10:00 - 22:00</div>
                    </li>
                    <li>
                        <div class="contacts__info_name">ТРК "КОЛЬЦО", ОТДЕЛ "УЗБИ" 1 ЭТАЖ</div>
                        <div class="contacts__info_address">Россия, г. Челябинск, ул Дарвина, 18</div>
                        <div class="contacts__info_worktime">Время работы: ежедневно с 10:00 - 22:00</div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
    <div class="contacts__map" id="map"></div>

</section>