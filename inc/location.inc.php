<div class="location">
    <a class="location__close location_close" href="#"></a>
    <div class="location__heading">
        <img src="img/logo__mobile.png" alt="">
    </div>
    <div class="location__title"><span class="color_green">В</span>ыберите свой город</div>
    <div class="location__form">
        <input class="location__input" type="text" name="location" value="Челябинск">
    </div>
    <div class="location__current">Ваш текущий город <span class="location__current_city">Челябинск</span>.</div>
    <div class="location__text">Чтобы изменить город, выберите из предложенных, либо введите название в поле выше и выберите свой город из предложенного списка.</div>
    <button type="button" class="btn location__button">выбрать</button>
</div>
<div class="location__layout location_close"></div>