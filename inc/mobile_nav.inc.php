<nav class="nav_mobile">
    <a class="nav_mobile__close nav_toggle" href="#"></a>
    <div class="nav_mobile__scroll">
        <div class="nav_mobile__content">
            <ul>
                <li>
                    <a href="#">Техника для кухни</a>
                    <ul>
                        <li><a href="#">Скороварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Соковарки</a></li>
                        <li><a href="#">Посуда</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Товары для кухни</a>
                    <ul>
                        <li><a href="#">Скороварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Соковарки</a></li>
                        <li><a href="#">Посуда</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Самогонные аппараты</a>
                    <ul>
                        <li><a href="#">Скороварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Соковарки</a></li>
                        <li><a href="#">Посуда</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Все для самогоноварения</a>
                    <ul>
                        <li><a href="#">Скороварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Соковарки</a></li>
                        <li><a href="#">Посуда</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Пивоварение</a>
                    <ul>
                        <li><a href="#">Скороварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Соковарки</a></li>
                        <li><a href="#">Посуда</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Фабрика домашних консервов</a>
                    <ul>
                        <li><a href="#">Скороварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Соковарки</a></li>
                        <li><a href="#">Посуда</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Бытовая техника для дома</a>
                    <ul>
                        <li><a href="#">Скороварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Соковарки</a></li>
                        <li><a href="#">Посуда</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Красота и здоровье</a>
                    <ul>
                        <li><a href="#">Скороварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Соковарки</a></li>
                        <li><a href="#">Посуда</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Товары для дачи и сада</a>
                    <ul>
                        <li><a href="#">Скороварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Соковарки</a></li>
                        <li><a href="#">Посуда</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Товары для хозяйства</a>
                    <ul>
                        <li><a href="#">Скороварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Соковарки</a></li>
                        <li><a href="#">Посуда</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Все для активного отдыха</a>
                    <ul>
                        <li><a href="#">Скороварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Мантоварки</a></li>
                        <li><a href="#">Соковарки</a></li>
                        <li><a href="#">Посуда</a></li>
                    </ul>
                </li>

                <li><a href="#">Уценка</a></li>
                <li><a href="#">Акции</a></li>
                <li><a href="#">Торговые марки</a></li>
                <li><a href="#">Оптовикам</a></li>

                <li><a href="#">О компании</a></li>
                <li><a href="#">Каталог</a></li>
                <li><a href="#">Доставка и оплата</a></li>
                <li><a href="#">Новости</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="nav_mobile__layout nav_toggle"></div>