
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Site name</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.2/css/swiper.min.css">
<link rel="stylesheet" href="js/vendor/mCustomScrollbar/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
<link rel="stylesheet" href="js/vendor/slinky/slinky.min.css">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

<link rel="stylesheet" href="fonts/HelveticaNeueCyr/stylesheet.css">
<link rel="stylesheet" href="fonts/UniSans/stylesheet.css">
<link rel="stylesheet" href="css/main.css">