<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.2/js/swiper.min.js"></script>
<script src="js/vendor/svg4everybody.legacy.min.js"></script>
<script src="js/vendor/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
<script src="js/vendor/slinky/slinky.min.js"></script>

<script src="js/plugins.js"></script>
<script src="js/main.js"></script>


<!-- Map -->
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script>
    ymaps.ready(init);

    function init () {
        var myMap = new ymaps.Map('map', {
                center: [55.163738980459286,61.313748245209126],
                zoom: 11,
                controls: ['smallMapDefaultSet']
            }),

            myPlacemark1 = new ymaps.Placemark([55.20482374846717,61.327289042327784], {
                balloonContent: '<strong>ТРК "ФИЕСТА", ОТДЕЛ "УЗБИ" 2 ЭТАЖ</strong><p>Россия, г. Челябинск, ул Молодогвардейцев, 7</p><p>ежедневно с 10:00 - 22:00</p>'
            }, {
                iconLayout: 'default#image',
                iconImageClipRect: [[0,0], [40, 54]],
                iconImageHref: 'img/placemark.png',
                iconImageSize: [40, 54],
                iconImageOffset: [-20, -54]
            }),

            myPlacemark2 = new ymaps.Placemark([55.17105106955312,61.35597599999991], {
                balloonContent: '<strong>ТРК "РОДНИК", ОТДЕЛ "УЗБИ" 2 ЭТАЖ</strong><p>Россия, г. Челябинск, ул Труда, 203</p><p>ежедневно с 10:00 - 22:00</p>'
            }, {
                iconLayout: 'default#image',
                iconImageClipRect: [[0,0], [40, 54]],
                iconImageHref: 'img/placemark.png',
                iconImageSize: [40, 54],
                iconImageOffset: [-20, -54]
            }),

            myPlacemark3 = new ymaps.Placemark([55.12684856958742,61.37010649999995], {
                balloonContent: '<strong>ТРК "КОЛЬЦО", ОТДЕЛ "УЗБИ" 1 ЭТАЖ</strong><p>Россия, г. Челябинск, ул Дарвина, 18</p><p>ежедневно с 10:00 - 22:00</p>'
            }, {
                iconLayout: 'default#image',
                iconImageClipRect: [[0,0], [40, 54]],
                iconImageHref: 'img/placemark.png',
                iconImageSize: [40, 54],
                iconImageOffset: [-20, -54]
            });

        myMap.geoObjects.add(myPlacemark1);
        myMap.geoObjects.add(myPlacemark2);
        myMap.geoObjects.add(myPlacemark3);
    }

</script>
<!-- -->