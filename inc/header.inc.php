<header class="header_mobile">
    <div class="header_mobile__container">
        <div class="header_mobile__left">

            <a class="header_mobile__toggle nav_toggle" href="#">
                <span></span>
                <span></span>
                <span></span>
            </a>

        </div>
        <a class="header_mobile__logo" href="/">
            <img src="img/logo__mobile.png"  alt="">
        </a>
        <div class="header_mobile__right">

            <a class="header_mobile__phone" href="tel:+7 (800) 550-18-14"><i class="fas fa-phone"></i><span>+7 (800) 550-18-14</span></a>

            <a class="header_mobile__cart" href="#">
                <span>3</span>
            </a>

        </div>
    </div>
</header>

<header class="header">
    <div class="header__top">
        <div class="header__container">
            <a class="header__location" href="#">
                <i>
                    <svg class="ico-svg" viewBox="0 0 18 24" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#icon__location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </i>
                <span>Челябинск</span>
            </a>
            <div class="header__info">
                <div class="header__phones"><span>Опт</span><a href="tel:+73512203989">+7 (351) 220-39-89</a>, <span>Розница</span><a href="tel:+78005501814">+7 (800) 550-18-14</a></div>
                <div class="header__worktime">Пн-Пт 9:00-18:00, выходные: Сб-Вс</div>
            </div>
        </div>
    </div>
    <div class="header__bottom">
        <div class="header__container">
            <ul class="header__nav">
                <li><a href="#">О компании</a></li>
                <li><a href="#">Каталог</a></li>
                <li><a href="#">Доставка и оплата</a></li>
                <li><a href="#">Новости</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
            <div class="header__menu">

                <a class="header__account" href="#">
                    <i>
                        <svg class="ico-svg" viewBox="0 0 19 22" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite_icons.svg#icon__user" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                    <span>Личный кабинет</span>
                </a>

                <div class="header__links">
                    <div class="header__links_wrap">
                        <a class="header__links_compare" href="#">

                        </a>
                        <a class="header__links_bookmark" href="#">
                            <span>5</span>
                        </a>
                        <a class="header__links_favorites" href="#">
                            <span>2</span>
                        </a>
                        <a class="header__links_cart" href="#">
                            <span>3</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>
<div class="header_label"></div>