<footer class="footer">

    <div class="footer__top">
        <div class="container">
            <div class="footer__top_row">
                <ul class="footer__menu">
                    <li><a href="#">О компании</a></li>
                    <li><a href="#">Каталог</a></li>
                    <li><a href="#">Доставка и оплата</a></li>
                    <li><a href="#">Новости</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
                <div class="footer__info">
                    <ul class="footer__info_phones">
                        <li><span>Опт</span><a href="tel:+73512203989">+7 (351) 220-39-89</a>,</li>
                        <li><span>Розница</span><a href="tel:+78005501814">+7 (800) 550-18-14</a></li>
                    </ul>
                    <div class="footer__info_worktime">Пн-Пт 9:00-18:00, выходные: Сб-Вс</div>
                </div>
            </div>
        </div>
    </div>

    <nav class="footer__nav">
        <div class="container">
            <div class="footer__nav_row">

                <div class="footer__nav_col">
                    <div class="footer__nav_block">
                        <div class="footer__nav_title"><a href="#">Бытовая техника для кухни</a></div>
                        <ul>
                            <li><a href="#">Духовые шкафы</a></li>
                            <li><a href="#">Грили</a></li>
                            <li><a href="#">Плиты</a></li>
                            <li><a href="#">Вафельницы, орешницы</a></li>
                            <li><a href="#">Мясорубки</a></li>
                            <li><a href="#">Мультиварки, пароварки</a></li>
                            <li><a href="#">Соковыжималки</a></li>
                            <li><a href="#">Микроволновые печи</a></li>
                            <li><a href="#">Блендеры и миксеры</a></li>
                            <li><a href="#">Кофемолки</a></li>
                            <li><a href="#">Весы</a></li>
                            <li><a href="#">Самовары и чайники</a></li>
                            <li><a href="#">Термопоты</a></li>
                            <li><a href="#">Термосы</a></li>
                            <li><a href="#">Сушилки для овощей, фруктов</a></li>
                            <li><a href="#">Чудо - печи</a></li>
                            <li><a href="#">Электрошашлычницы</a></li>
                        </ul>
                    </div>
                </div>

                <div class="footer__nav_col">
                    <div class="footer__nav_block">
                        <div class="footer__nav_title"><a href="#">Товары для кухни</a></div>
                        <ul>
                            <li><a href="#">Скороварки</a></li>
                            <li><a href="#">Мантоварки</a></li>
                            <li><a href="#">Соковарки</a></li>
                            <li><a href="#">Посуда</a></li>
                        </ul>
                    </div>
                    <div class="footer__nav_block">
                        <div class="footer__nav_title"><a href="#">Самогонные аппараты Первач</a></div>
                    </div>
                    <div class="footer__nav_block">
                        <div class="footer__nav_title"><a href="#">Фабрика домашних консервов</a></div>
                        <ul>
                            <li><a href="#">Автоклавы</a></li>
                            <li><a href="#">Закаточные машинки</a></li>
                        </ul>
                    </div>
                    <div class="footer__nav_block">
                        <div class="footer__nav_title"><a href="#">Все для активного отдыха</a></div>
                        <ul>
                            <li><a href="#">Санки, коляски</a></li>
                        </ul>
                    </div>
                </div>

                <div class="footer__nav_col">
                    <div class="footer__nav_block">
                        <div class="footer__nav_title"><a href="#">Бытовая техника для дома</a></div>
                        <ul>

                            <li><a href="#">Обогреватели</a></li>
                            <li><a href="#">Увлажнители воздуха</a></li>
                            <li><a href="#">Утюги, паровые станции</a></li>
                            <li><a href="#">Пылесосы</a></li>
                            <li><a href="#">Стиральные машины</a></li>
                            <li><a href="#">Сетевые фильтры</a></li>
                        </ul>
                    </div>
                    <div class="footer__nav_block">
                        <div class="footer__nav_title"><a href="#">Красота и здоровье</a></div>
                        <ul>
                            <li><a href="#">Бритвы</a></li>
                            <li><a href="#">Машинки для стрижки</a></li>
                            <li><a href="#">Весы напольные</a></li>
                            <li><a href="#">Товары для здоровья</a></li>
                        </ul>
                    </div>
                </div>

                <div class="footer__nav_col">
                    <div class="footer__nav_block">
                        <div class="footer__nav_title"><a href="#">Товары для дачи и сада</a></div>
                        <ul>
                            <li><a href="#">Коптильни</a></li>
                            <li><a href="#">Щепа для копчения</a></li>
                            <li><a href="#">Садово-огородный инвентарь</a></li>
                            <li><a href="#">Насосы</a></li>
                            <li><a href="#">Шланги поливочные</a></li>
                            <li><a href="#">Умывальники</a></li>
                            <li><a href="#">Тележки</a></li>
                            <li><a href="#">Кипятильники</a></li>
                            <li><a href="#">Насосные станции</a></li>
                            <li><a href="#">Товары для подсобного хозяйства</a></li>
                            <li><a href="#">Сепараторы</a></li>
                            <li><a href="#">Зернодробилки</a></li>
                            <li><a href="#">Инкубаторы</a></li>
                            <li><a href="#">Лампы паяльные, горелки</a></li>
                            <li><a href="#">Кормоизмельчители</a></li>
                            <li><a href="#">Маслобойки</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </nav>


    <div class="footer__bottom">
        <div class="container">
            <div class="footer__bottom_row">

                <a class="footer__logo" href="">
                    <img src="img/footer__logo.png" class="img-fluid" alt="">
                </a>

                <div class="footer__contact">
                    <p>454012, Россия, г. Челябинск, Копейское шоссе, 9-П</p>
                    <p>Тел.: <a href="tel:88005501814">8 (800) 550-18-14</a></p>
                    <p>e-mail: <a href="mailto:info@marchencko.ru">info@marchencko.ru</a></p>
                    <p>ИП Марченко Александр Анатольевич</p>
                    <p>ИНН 744712613337</p>
                    <p>ОГРНИП 314745218500031</p>
                </div>

                <a class="footer__dev">
                    <img src="img/dev__logo.png" class="img-fluid" alt="">
                </a>

            </div>
        </div>
    </div>
</footer>