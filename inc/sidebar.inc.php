<aside class="sidebar">
    <nav class="sidenav">

        <div class="sidenav__wrap nav__scroll">

            <div class="sidenav__heading">
                <a class="sidebar__logo" href="#" title="Site name">
                    <img src="img/logo.png" class="img-fluid" alt="">
                </a>
                <a class="sidebar__logo_sm" href="#" title="Site name">
                    <img src="img/logo_sm.png" class="img-fluid" alt="">
                </a>
            </div>

            <div class="sidenav__search">
                <div class="sidenav__search_icon">
                    <i>
                        <svg class="ico-svg" viewBox="0 0 25 25" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite_icons.svg#icon__search" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                    <a href="#" class="sidenav__search_toggle"></a>
                </div>
                <div class="sidenav__search_form">
                    <form class="form">
                        <input type="text" name="name" class="sidenav__search_input" placeholder="Поиск">
                    </form>
                </div>
            </div>

            <ul class="sidenav__primary">

                <li class="item_01">
                    <div class="sidenav__primary_item sidenav__primary_dropdown" data-target="#sidenav__second_01">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 40 26" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_01" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Техника для кухни</span></div>
                    </div>
                    <a class="sidenav__primary_link" href="#"></a>
                </li>

                <li class="item_02">
                    <div class="sidenav__primary_item sidenav__primary_dropdown" data-target="#sidenav__second_02">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 39 25" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_02" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Товары для кухни</span></div>
                    </div>
                    <a class="sidenav__primary_link" href="#"></a>
                </li>

                <li class="item_03">
                    <div class="sidenav__primary_item sidenav__primary_dropdown" data-target="#sidenav__second_03">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 37 41" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_03" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Самогонные аппараты</span></div>
                    </div>
                    <a class="sidenav__primary_link" href="#"></a>
                </li>

                <li class="item_04">
                    <div class="sidenav__primary_item sidenav__primary_dropdown" data-target="#sidenav__second_04">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 35 35" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_04" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Все для самогоноварения</span></div>
                    </div>
                    <a class="sidenav__primary_link" href="#"></a>
                </li>

                <li class="item_05">
                    <div class="sidenav__primary_item sidenav__primary_dropdown" data-target="#sidenav__second_05">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 31 33" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_05" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Пивоварение</span></div>
                    </div>
                    <a class="sidenav__primary_link" href="#"></a>
                </li>

                <li class="item_06">
                    <div class="sidenav__primary_item sidenav__primary_dropdown" data-target="#sidenav__second_06">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 38 37" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Фабрика домашних консервов</span></div>
                    </div>
                    <a class="sidenav__primary_link" href="#"></a>
                </li>

                <li class="item_07">
                    <div class="sidenav__primary_item sidenav__primary_dropdown" data-target="#sidenav__second_07">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 41 20" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_07" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Бытовая техника для дома</span></div>
                    </div>
                    <a class="sidenav__primary_link" href="#"></a>
                </li>

                <li class="item_08">
                    <div class="sidenav__primary_item sidenav__primary_dropdown" data-target="#sidenav__second_08">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 24 40" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_08" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Красота и здоровье</span></div>
                    </div>
                    <a class="sidenav__primary_link" href="#"></a>
                </li>

                <li class="item_09">
                    <div class="sidenav__primary_item sidenav__primary_dropdown" data-target="#sidenav__second_09">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 38 40" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_09" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Товары для дачи и сада</span></div>
                    </div>
                    <a class="sidenav__primary_link" href="#"></a>
                </li>

                <li class="item_10">
                    <div class="sidenav__primary_item sidenav__primary_dropdown" data-target="#sidenav__second_10">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 35 36" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_10" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Товары для хозяйства</span></div>
                    </div>
                    <a class="sidenav__primary_link" href="#"></a>
                </li>

                <li class="item_11">
                    <div class="sidenav__primary_item sidenav__primary_dropdown" data-target="#sidenav__second_11">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 39 18" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_11" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Все для активного отдыха</span></div>
                    </div>
                    <a class="sidenav__primary_link" href="#"></a>
                </li>

            </ul>

            <ul class="sidenav__primary">

                <li class="item_12">
                    <a href="#" class="sidenav__primary_item sidenav__primary_green">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 30 30" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_12" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Уценка</span></div>
                    </a>
                </li>

                <li class="item_13">
                    <a href="#" class="sidenav__primary_item sidenav__primary_green">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 30 31" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_13" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Акции</span></div>
                    </a>
                </li>

                <li class="item_14">
                    <a href="#" class="sidenav__primary_item sidenav__primary_green">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 28 32" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_14" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Торговые марки</span></div>
                    </a>
                </li>

                <li class="item_15">
                    <a href="#" class="sidenav__primary_item sidenav__primary_green">
                        <div class="sidenav__primary_icon">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 27 27" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__nav_15" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="sidenav__primary_text"><span>Оптовикам</span></div>
                    </a>
                </li>
            </ul>

        </div>

        <div class="sidenav__search_slide">
            <form class="form">
                <input type="text" name="name" class="sidenav__search_input" placeholder="Поиск">
            </form>
        </div>

        <div class="sidenav__second" id="sidenav__second_01">
            <div class="sidenav__second_wrap">
                <div class="sidenav__second_heading"><span>Т</span>ехника для кухни</div>
                <a href="#" class="sidenav__second_close"></a>
                <ul>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Скороварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_01.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_02.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_03.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Соковарки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_04.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_05.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_06.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="sidenav__second" id="sidenav__second_02">
            <div class="sidenav__second_wrap">
                <div class="sidenav__second_heading"><span>Т</span>овары для кухни</div>
                <a href="#" class="sidenav__second_close"></a>
                <ul>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Скороварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_01.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_02.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_03.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Соковарки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_04.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_05.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_06.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="sidenav__second" id="sidenav__second_03">
            <div class="sidenav__second_wrap">
                <div class="sidenav__second_heading"><span>С</span>амогонные аппараты</div>
                <a href="#" class="sidenav__second_close"></a>
                <ul>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Скороварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_01.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_02.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_03.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Соковарки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_04.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_05.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_06.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="sidenav__second" id="sidenav__second_04">
            <div class="sidenav__second_wrap">
                <div class="sidenav__second_heading"><span>В</span>се для самогоноварения</div>
                <a href="#" class="sidenav__second_close"></a>
                <ul>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Скороварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_01.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_02.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_03.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Соковарки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_04.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_05.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_06.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="sidenav__second" id="sidenav__second_05">
            <div class="sidenav__second_wrap">
                <div class="sidenav__second_heading"><span>П</span>ивоварение</div>
                <a href="#" class="sidenav__second_close"></a>
                <ul>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Скороварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_01.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_02.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_03.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Соковарки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_04.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_05.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_06.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="sidenav__second" id="sidenav__second_06">
            <div class="sidenav__second_wrap">
                <div class="sidenav__second_heading"><span>Ф</span>абрика домашних консервов</div>
                <a href="#" class="sidenav__second_close"></a>
                <ul>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Скороварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_01.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_02.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_03.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Соковарки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_04.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_05.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_06.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="sidenav__second" id="sidenav__second_07">
            <div class="sidenav__second_wrap">
                <div class="sidenav__second_heading"><span>Б</span>ытовая техника для дома</div>
                <a href="#" class="sidenav__second_close"></a>
                <ul>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Скороварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_01.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_02.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_03.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Соковарки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_04.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_05.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_06.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="sidenav__second" id="sidenav__second_08">
            <div class="sidenav__second_wrap">
                <div class="sidenav__second_heading"><span>К</span>расота и здоровье</div>
                <a href="#" class="sidenav__second_close"></a>
                <ul>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Скороварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_01.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_02.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_03.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Соковарки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_04.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_05.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_06.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="sidenav__second" id="sidenav__second_09">
            <div class="sidenav__second_wrap">
                <div class="sidenav__second_heading"><span>Т</span>овары для дачи и сада</div>
                <a href="#" class="sidenav__second_close"></a>
                <ul>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Скороварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_01.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_02.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_03.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Соковарки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_04.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_05.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_06.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="sidenav__second" id="sidenav__second_10">
            <div class="sidenav__second_wrap">
                <div class="sidenav__second_heading"><span>Т</span>овары для хозяйства</div>
                <a href="#" class="sidenav__second_close"></a>
                <ul>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Скороварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_01.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_02.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_03.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Соковарки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_04.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_05.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_06.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="sidenav__second" id="sidenav__second_11">
            <div class="sidenav__second_wrap">
                <div class="sidenav__second_heading"><span>В</span>се для активного отдыха</div>
                <a href="#" class="sidenav__second_close"></a>
                <ul>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Скороварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_01.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_02.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Мантоварки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_03.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Соковарки</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_04.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_05.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="sidenav__second_name">Посуда</div>
                            <div class="sidenav__second_image">
                                <img src="images/sidenav__image_06.jpg" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

    </nav>
</aside>