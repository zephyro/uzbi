$(document).ready(function() {

    // SVG IE11 support
    svg4everybody();

    const mobileNav = $('.nav_mobile__content').slinky();

    $(function($){
        var topnav = $('.header');
        var label = $('.header_label');
        $h = label.offset().top;

        $(window).scroll(function(){

            if ( $(window).scrollTop() > $h) {
                topnav.addClass('fix_top');
            }else{
                topnav.removeClass('fix_top');
            }
        });
    });

    // nav
    (function() {

        $('.sidenav__primary_dropdown').on('click touchstart', function(e){
            e.preventDefault();

            var second = $($(this).attr("data-target"));

            $(this).closest('.sidenav__primary').find('.sidenav__primary_dropdown').removeClass('active');
            $(this).addClass('active');

            $('.sidenav').find('> .sidenav__second').removeClass('active');
            $('.sidenav').find(second).addClass('active');
        });

        $('.sidenav__second_close').on('click touchstart', function(e){
            e.preventDefault();

            $(this).closest('.sidenav__second').removeClass('active');
            $('.sidenav').find('.sidenav__primary_dropdown').removeClass('active');

        });

    }());

    $('.nav_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.nav_mobile').toggleClass('open');
        $('.nav_mobile__layout').toggleClass('open');
    });


    $('body').click(function (event) {

        if ($(event.target).closest(".sidenav").length === 0) {
            $(".sidenav__second").removeClass('active');
        }
    });


// Scroll nav
    $(".nav__scroll").mCustomScrollbar({
        theme:"minimal-dark",
        scrollInertia:700
    });

    $(".btn_modal").fancybox({
        'padding'    : 0
    });

    var promo = new Swiper('.promo__slider', {
        loop: true,
        navigation: {
            nextEl: '.promo__button_next',
            prevEl: '.promo__button_prev',
        },
    });

    $('.sidenav__search_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.sidenav__search_slide').toggleClass('open');
    });


    $('.header__location').on('click touchstart', function(e){
        e.preventDefault();
        $('.location').addClass('open');
        $('.location__layout').addClass('open');
    });

    $('.location_close').on('click touchstart', function(e){
        e.preventDefault();
        $('.location').removeClass('open');
        $('.location__layout').removeClass('open');
    });

    // Slider
    var  showcase__slider_one = new Swiper('.showcase__slider_one', {
        slidesPerView: 'auto',
        spaceBetween: 0,
        loop: true,
        navigation: {
            nextEl: '.slider_arrow_next',
            prevEl: '.slider_arrow_prev',
        }
    });

    // Tabs
    $('.tabs_nav  li a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("href"));
        var box = $(this).closest('.tabs');

        console.log(tab);

        $(this).closest('.tabs_nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        box.find('.tabs_item').removeClass('active');
        box.find(tab).addClass('active');

    });

    // Brand Slider
    var brand = new Swiper('.brand__slider', {
        spaceBetween: 0,
        slidesPerView: 4,
        loop: true,
        navigation: {
            nextEl: '.slider_arrow_next',
            prevEl: '.slider_arrow_prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 3,
            },
            576: {
                slidesPerView: 2,
            }
        }
    });


    // Slider
    var  showcase__slider_two = new Swiper('.showcase__slider_two', {
        slidesPerView: 'auto',
        spaceBetween: 0,
        loop: true,
        navigation: {
            nextEl: '.slider_arrow_next',
            prevEl: '.slider_arrow_prev',
        }
    });



    // Gallery
    var gallery_slider = new Swiper('.gallery_slider', {
        pagination: {
            el: '.swiper-pagination',
            type: 'progressbar',
        }
    });

    var gallery_thumb = new Swiper('.gallery_thumb', {
        slidesPerView: 3,
        spaceBetween: 5,
        direction: 'vertical',
        navigation: {
            nextEl: '.thumb_arrow_next',
            prevEl: '.thumb_arrow_prev',
        },
    });


    $('.product__media_thumb').on('click touchstart', function(e){
        e.preventDefault();

        var slide = $(this).attr("data-target");

        console.log(slide);

        $('.product__media').find('.product__media_thumb').removeClass('active');
        $(this).addClass('active');

        gallery_slider.slideTo(slide);
    });




    $('.product_data__heading').on('click touchstart', function(e){
        e.preventDefault();

        $(this).closest('.product_data').toggleClass('active');

    });

    $('.product_data__toggle').on('click touchstart', function(e){
        e.preventDefault();

        $(this).closest('.product_data__params').find('.product_data__hide').toggleClass('open');
        $(this).toggleClass('open');

    });

    $('.product_tabs__nav  li a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("href"));
        var box = $(this).closest('.product_tabs');

        console.log(tab);

        $(this).closest('.product_tabs__nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        box.find('.product_tabs__item').removeClass('active');
        box.find(tab).addClass('active');

    });

    // Slider
    var  review = new Swiper('.review_slider', {
        slidesPerView: 'auto',
        spaceBetween: 0,
        loop: true,
        navigation: {
            nextEl: '.slider_arrow_next',
            prevEl: '.slider_arrow_prev',
        }
    });

});


