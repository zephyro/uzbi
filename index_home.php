<!doctype html>
<html class="no-js" lang="">
    <head>

        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->

        <link rel="stylesheet" href="css/homepage.css">

    </head>
    <body>

        <div class="layout">

            <!-- Location -->
            <?php include('inc/location.inc.php') ?>
            <!-- -->

            <!-- Mobile Nav -->
            <?php include('inc/mobile_nav.inc.php') ?>
            <!-- -->

            <!-- Sidebar -->
            <?php include('inc/sidebar.inc.php') ?>
            <!-- -->

            <div class="page">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <div class="promo">
                    <div class="promo__slider swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="promo__item" style="background-image: url('img/slide.jpg')"></div>
                            </div>
                            <div class="swiper-slide">
                                <div class="promo__item" style="background-image: url('img/slide.jpg')"></div>
                            </div>
                            <div class="swiper-slide">
                                <div class="promo__item" style="background-image: url('img/slide.jpg')"></div>
                            </div>
                        </div>
                        <!-- Add Arrows -->
                        <div class="promo__button_next">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 26 48" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__promo_arrow_next" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="promo__button_prev">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 26 48" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__promo_arrow_prev" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                    </div>
                </div>

                <section class="showcase tabs">
                    <div class="container">
                        <div class="h2"><span class="color_green">П</span>окупают каждый день<sup class="color_green">26</sup></div>
                        <ul class="showcase__nav tabs_nav">
                            <li class="active"><a href="#tab1">Коптильни</a></li>
                            <li><a href="#tab2">Духовые шкафы</a></li>
                            <li><a href="#tab3">Автоклавы</a></li>
                        </ul>
                    </div>
                    <div class="showcase__slider_wrap">
                        <div class="container">
                            <div class="showcase__wrap">

                                <div class="tabs_item tabs_item_start active" id="tab1">

                                    <div class="showcase__slider showcase__slider_one swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Add Arrows -->
                                        <div class="slider_arrow_prev">
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>
                                        <div class="slider_arrow_next">
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__angle_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>

                                    </div>

                                </div>

                                <div class="tabs_item" id="tab2">

                                    <div class="showcase__slider  showcase__slider_one swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Add Arrows -->
                                        <div class="slider_arrow_prev">
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>
                                        <div class="slider_arrow_next">
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__angle_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>

                                    </div>

                                </div>

                                <div class="tabs_item" id="tab3">

                                    <div class="showcase__slider  showcase__slider_one swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide goods">
                                                <div class="goods__wrap">
                                                    <div class="goods__action">
                                                        <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                        <a href="#" class="goods__action_item goods__action_compare"></a>
                                                        <a href="#" class="goods__action_item goods__action_search"></a>
                                                    </div>
                                                    <div class="goods__sticky">
                                                        <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                        <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                        <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                    </div>
                                                    <a href="#" class="goods__image" title="">
                                                        <img src="images/goods__image.jpg" alt="">
                                                    </a>
                                                    <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                    <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                    <div class="goods__price">
                                                        <div class="goods__price_old">6 499</div>
                                                        <div class="goods__price_new">5 849₽</div>
                                                        <div class="goods__price_discount">-10%</div>
                                                    </div>
                                                    <div class="goods__hide">
                                                        <div class="goods__logo">
                                                            <img src="images/goods__logo.png" alt="">
                                                        </div>
                                                        <div class="goods__data">
                                                            <ul>
                                                                <li><span>Объем</span></li>
                                                                <li><span>20л</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Сухопарник</span></li>
                                                                <li><span>да</span></li>
                                                            </ul>
                                                            <ul>
                                                                <li><span>Термометр</span></li>
                                                                <li><span>нет</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="text-center">
                                                            <a href="#" class="btn">купить</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Add Arrows -->
                                        <div class="slider_arrow_prev">
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>
                                        <div class="slider_arrow_next">
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__angle_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </section>

                <section class="brand">
                    <div class="container">
                        <div class="h2"><span>Т</span>орговые марки</div>
                        <div class="brand__slider swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="brand__item">
                                        <img src="images/brand__logo_01.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="brand__item">
                                        <img src="images/brand__logo_02.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="brand__item">
                                        <img src="images/brand__logo_03.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="brand__item">
                                        <img src="images/brand__logo_04.png" class="img-fluid" alt="">
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="brand__item">
                                        <img src="images/brand__logo_01.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="brand__item">
                                        <img src="images/brand__logo_02.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="brand__item">
                                        <img src="images/brand__logo_03.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="brand__item">
                                        <img src="images/brand__logo_04.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                            <!-- Add Arrows -->
                            <div class="slider_arrow_prev">
                                <i>
                                    <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </div>
                            <div class="slider_arrow_next">
                                <i>
                                    <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="bnr">
                    <div class="bnr__image">
                        <img src="img/bnr__image.png" alt="" class="img-fluid">
                    </div>
                    <div class="container">
                        <div class="bnr__row">
                            <div class="bnr__col">
                                <div class="bnr__text"><span>Купи мультиварку  получи вторую чашу</span></div>
                            </div>
                        </div>
                    </div>
                </div>

                <section class="showcase">
                    <div class="container">
                        <div class="h2"><span class="color_green">У</span>цененные товары<sup class="color_green">19</sup></div>
                    </div>
                    <div class="showcase__slider_wrap">
                        <div class="container">
                            <div class="showcase__wrap">
                                <div class="showcase__slider showcase__slider_two swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                    <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                    <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                    <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide goods">
                                            <div class="goods__wrap">
                                                <div class="goods__action">
                                                    <a href="#" class="goods__action_item goods__action_favorite"></a>
                                                    <a href="#" class="goods__action_item goods__action_compare"></a>
                                                    <a href="#" class="goods__action_item goods__action_search"></a>
                                                </div>
                                                <div class="goods__sticky">
                                                    <div class="goods__sticky_item goods__sticky_blue">Уценка</div>
                                                    <div class="goods__sticky_item goods__sticky_green">хит</div>
                                                    <div class="goods__sticky_item goods__sticky_yellow">Акция</div>
                                                </div>
                                                <a href="#" class="goods__image" title="">
                                                    <img src="images/goods__image.jpg" alt="">
                                                </a>
                                                <div class="goods__type">Сушилки для овощей, фруктов</div>
                                                <div class="goods__name">Электросушилка Ветерок-5, прозрачный</div>
                                                <div class="goods__price">
                                                    <div class="goods__price_old">6 499</div>
                                                    <div class="goods__price_new">5 849₽</div>
                                                    <div class="goods__price_discount">-10%</div>
                                                </div>
                                                <div class="goods__hide">
                                                    <div class="goods__logo">
                                                        <img src="images/goods__logo.png" alt="">
                                                    </div>
                                                    <div class="goods__data">
                                                        <ul>
                                                            <li><span>Объем</span></li>
                                                            <li><span>20л</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Сухопарник</span></li>
                                                            <li><span>да</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Термометр</span></li>
                                                            <li><span>нет</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="text-center">
                                                        <a href="#" class="btn">купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="slider_arrow_prev">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="slider_arrow_next">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 16 28" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__angle_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="videos">
                    <div class="container">
                        <div class="h2"><span class="color_green">С</span>амое интересное и полезное видео</div>
                        <div class="videos__wrap">
                            <div class="videos__row">
                                <div class="videos__col">
                                    <a href="https://www.youtube.com/watch?v=jYAYn7jiP4k&t=2s" class="videos__item" data-fancybox>
                                        <span>Коптильня холодного копчения ДымДымыч</span>
                                        <img src="images/video__xl.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="videos__col">
                                    <div class="videos__row">
                                        <div class="videos__thumb">
                                            <a href="https://www.youtube.com/watch?v=jYAYn7jiP4k&t=2s" class="videos__item" data-fancybox>
                                                <span>Коптильня холодного копчения ДымДымыч</span>
                                                <img src="images/video__xl.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="videos__thumb">
                                            <a href="https://www.youtube.com/watch?v=jYAYn7jiP4k&t=2s" class="videos__item" data-fancybox>
                                                <span>Коптильня холодного копчения ДымДымыч</span>
                                                <img src="images/video__xl.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="videos__thumb">
                                            <a href="https://www.youtube.com/watch?v=jYAYn7jiP4k&t=2s" class="videos__item" data-fancybox>
                                                <span>Коптильня холодного копчения ДымДымыч</span>
                                                <img src="images/video__xl.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="videos__thumb">
                                            <a href="https://www.youtube.com/watch?v=jYAYn7jiP4k&t=2s" class="videos__item" data-fancybox>
                                                <span>Коптильня холодного копчения ДымДымыч</span>
                                                <img src="images/video__xl.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="comments">
                    <div class="container">
                        <div class="h2"><span class="color_green">О</span>тзывы наших покупателей</div>
                        <div class="videos__wrap">
                            <div class="videos__row">
                                <div class="videos__col">
                                    <div class="videos__row">
                                        <div class="videos__thumb">
                                            <a href="https://www.youtube.com/watch?v=jYAYn7jiP4k&t=2s" class="videos__item" data-fancybox>
                                                <img src="images/video__xl.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="videos__thumb">
                                            <div class="comments__item">
                                                <img src="img/commnets__bg_text.png" class="img-fluid" alt="">
                                                <div class="comments__wrap">
                                                    <div class="comments__date">24.05.2018</div>
                                                    <div class="comments__name">Алексей</div>
                                                    <div class="comments__text">Разнообразный и богатый опыт постоянное информационно-пропагандистское обеспечение нашей деятельности требуют от нас анализа позиций, занимаемых </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="videos__thumb">
                                            <div class="comments__item">
                                                <img src="img/commnets__bg_text.png" class="img-fluid" alt="">
                                                <div class="comments__wrap comments__wrap_first">
                                                    <div class="comments__date">24.05.2018</div>
                                                    <div class="comments__name">Алексей</div>
                                                    <div class="comments__text">Разнообразный и богатый опыт постоянное информационно-пропагандистское обеспечение нашей деятельности требуют от нас анализа позиций, занимаемых </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="videos__thumb">
                                            <a href="https://www.youtube.com/watch?v=jYAYn7jiP4k&t=2s" class="videos__item" data-fancybox>
                                                <img src="images/video__xl.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="videos__col">
                                    <a href="https://www.youtube.com/watch?v=jYAYn7jiP4k&t=2s" class="videos__item" data-fancybox>
                                        <img src="images/video__xl.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="about">
                    <div class="container">
                        <div class="h2"><span class="color_green">П</span>риветствуем вас</div>
                        <div class="about__lead">НА ОФИЦИАЛЬНОМ САЙТЕ УРАЛЬСКОГО ЗАВОДА БЫТОВЫХ ИЗДЕЛИЙ</div>
                        <p>Российские производители техники занимают на рынке лидирующие позиции, так как их продукция отличается высоким качеством и надежностью. Так и Уральский Завод по производству бытовых изделий гарантирует высококачественное изготовление товаров для дома. Более 20 лет мы обеспечиваем покупателей качественным и недорогим оборудованием, необходимым в повседневной жизни.</p>
                        <ul class="about__info">
                            <li>
                                <div class="about__info_image">
                                    <i>
                                        <img src="img/about__image__01.svg" class="img-fluid" alt="">
                                    </i>
                                </div>
                                <div class="about__info_text">РОССИЙСКИЙ<br/>ПРОИЗВОДИТЕЛЬ</div>
                            </li>
                            <li>
                                <div class="about__info_image">
                                    <i>
                                        <img src="img/about__image__02.png" class="img-fluid" alt="">
                                    </i>
                                </div>
                                <div class="about__info_text">ГАРАНТИЯ<br/>КАЧЕСТВА</div>
                            </li>
                            <li>
                                <div class="about__info_image">
                                    <i>
                                        <img src="img/about__image__03.svg" class="img-fluid" alt="">
                                    </i>
                                </div>
                                <div class="about__info_text">БЕЗУПРЕЧНЫЙ<br/>СЕРВИС</div>
                            </li>
                            <li>
                                <div class="about__info_image">
                                    <i>
                                        <img src="img/about__image__04.svg" class="img-fluid" alt="">
                                    </i>
                                </div>
                                <div class="about__info_text">БОЛЕЕ 15 ЛЕТ<br/>НА РЫНКАХ РОССИИ<br/>И СНГ</div>
                            </li>
                            <li>
                                <div class="about__info_image">
                                    <i>
                                        <img src="img/about__image__05.svg" class="img-fluid" alt="">
                                    </i>
                                </div>
                                <div class="about__info_text">СВЫШЕ 30<br/>СЕРВИСНЫХ ЦЕНТРОВ<br/>ПО РОССИИ</div>
                            </li>
                        </ul>
                        <div class="about__row">
                            <div class="about__text">
                                <div class="about__text_wrap">
                                    <div class="h2"><span class="color_green">А</span>ссортимент товаров</div>
                                    <p>Мы официальный производитель (Россия), предоставляем потребителям огромный выбор следующих категорий товаров:</p>
                                    <p>
                                        Техника для кухни является неотъемлемыми элементами для любой хозяйки. Сюда можно отнести самые популярные товары в интернете - это духовые шкафы, вафельницы, мультиварки, мясорубки, весы и прочее.<br/>
                                        Бытовая техника есть в каждом доме. Такими помощниками по дому служат пылесосы, утюги, обогреватели, стиральные машины и так далее. Наш завод УЗБИ РФ предоставляет гарантии на каждый из представленных товаров. <br/>
                                        Товары для кухни необходимы в приготовлении пищи и хранении продуктов. Такая продукция уральского завода бытовых изделий включает в себя мантоварки, скороварки, соковарки, крышки и многое другое.<br/>
                                    </p>
                                </div>
                                <a href="#" class="btn">подробнее</a>
                            </div>
                            <div class="about__image">
                                <img src="img/about__image.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Contacts -->
                <?php include('inc/contacts.inc.php') ?>
                <!-- -->

                <!-- Footer -->
                <?php include('inc/footer.inc.php') ?>
                <!-- -->

            </div>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
